from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, TIMESTAMP

Base = declarative_base()


class MissingPackage(Base):
    __tablename__='missing_package'

    id = Column(Integer, primary_key=True, autoincrement=True)
    bag_order = Column(Integer)
    package_order = Column(Integer)
    ticket_id = Column(Integer)
    is_cancel = Column(Integer)
    created = Column(TIMESTAMP)
    modified = Column(TIMESTAMP)
    
