import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

README = ""

requires = [
    'sqlalchemy'
]

setup(
      name='GHTK',
      version='0.1',
      description='GHTK',
      long_description=README,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
      ],
      author='',
      author_email='',
      keywords='',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      entry_points={}
)
